--CREATE DATABASE poke_teams;
--CREATE USER poke_user WITH PASSWORD 'poke_pword';
--GRANT ALL PRIVILEGES ON DATABASE "poke_teams" to poke_user;

--\c poke_teams

CREATE SCHEMA product;

CREATE TABLE product.teams (
  id        SERIAL PRIMARY KEY,
  name      CHARACTER VARYING (32) not null,
  create_ts TIMESTAMP default now(),
  update_ts TIMESTAMP default now()
);

-- cached poke
CREATE TABLE product.pokemon (
  id         SERIAL PRIMARY KEY,
  name       text unique not null,
  image      text not null,
  base_experience integer not null,
  abilities  text[],
  types      text[]
);

CREATE TABLE product.team_pokemon (
    poke_id integer not null,
    team_id integer not null
);
