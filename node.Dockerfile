FROM node:12-alpine

WORKDIR /src/

COPY ./src/backend/package.json package.json

RUN npm install

# for product
COPY ./src/backend/. .

EXPOSE 8080

# CMD npm run start
# for product
CMD npm run prod
