# Poke project - S'NCE GROUP test #

Pokemon Teams
 - Create your team, find pokemon and add in your team

### What is this repository for? ###

* 3 docker: web, database, api
* **Database**: postgresql 11
* **Frontend**: HTML5 with Bootstrap 4.4 CSS framework, jQuery 3.4 for javascript
* **Backend**: NodeJS with *Express* framework
* Version 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do you get set up and play? ###

* Download project
* In your terminal digit this command $: **docker-composer up**
* *if you have same error, use --build arg* **docker-composer up --build**
* Wait the completing process
* Open browser, go to url: http://127.0.0.1
* Click on **New Team** on the right top navbar
* Write your team name and press **OK**
* Click on **Gotta Catch 'Em All** button, you wait while searching pokemon and download data
* Click some time on Button **Gotta Catch 'Em All**, when you finish, return in Home page
* In **Homepage** you can watch all teams sorted by create time, you can filter all by select in top of table
* Click on team name for edit the team, you can remove and adding pokemon
* Thank you for play my project

### Other info ###

*This solution with 3 docker it's a good structure for scaled project*

#### Docker composed by: ####
|            |      |            |       |            |
| docker DB  |<---->| docker api |--api->| docker web |
| (postgres) |      |  (nodejs)  |       |  (nginx)   |
|            |      |            |       |            |


#### API list: ####
- [GET]  api/teams                   # get list of teams
- [GET]  api/teams?filter=type:a,b   # get list of teams filtered by <field>:<value>
- [POST] api/team/id                 # add new team
- [PUT]  api/team/id                 # delete team
- [GET]  api/random-pokemon          # get random pokemon data (and saved in team)

- [GET]  asset/images/img-id         # get image from stored folder

todo: add pagination for teams list

page requested:
- /team/create
    page creator team, with pokemon action button
- /team/<teamId>/edit
    page edit and update team
- /team/list
    page list teams, get all teams or filtered teams
    data team show:  team name, poke images list, poke sum experience, list types
    sorted by datetime create
    filters: pokemon types
    must use cache page

requested url pokemon:
- http://pokeapi.co/api/v2/pokemon/     # get number of pokemons and id
- http://pokeapi.co/api/v2/pokemon/<id> # get pokemon data

Missing in project (TODO)
- missing errors management
- missing validation forms
