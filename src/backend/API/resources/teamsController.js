 var {teams,pokemon} = require('../../models/models.js');
var cache = require('memory-cache');
// Display list of all Authors.
exports.team_list = function(req, res) {

  // cache
  var data = cache.get('teams-list')

  // get single team
  if(req.params.id){
     if(data && data.filter(({id})=>(id==req.params.id))){ //get from cache
       res.json({ success: true, data: data.filter(({id})=>(id==req.params.id))[0] })
       return
     }
    teams.get_team(req.params.id).then(t=>{
        res.json({ success: true, data: t })
    })
    return;
  }

  if(data){ //get from cache
    res.json({ success: true, data: data })
    return
  }
  teams.get_all().then(t=>{
    cache.put('teams-list', t)
    res.json({ success: true, data: t }) })
};

exports.team_new = function(req, res) {
  cache.del('teams-list')
  teams.insert_team(req.body.name).then(id=>{
    res.json({success: true, id:id})
  })
};

exports.team_edit = function(req, res) {
  cache.del('teams-list')
  pokemon.del_poke(req.body.pokeid, req.params.id)
  res.json({success: true });

};

exports.team_delete = function(req, res) {
    // # TODO: development body
    res.send('NOT IMPLEMENTED');
};
