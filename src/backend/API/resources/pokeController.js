var request = require('request');
var cache = require('memory-cache');
var {download} = require('../../helper/requests')
 var {pokemon, teams} = require('../../models/models.js');

var request_data = ['id','name','sprites','abilities','base_experience','types']

exports.random_poke = async function(req, res) {
    //find 807 pokemon ids
    // TODO: refresh number of pokemn from api
    // request('http://pokeapi.co/api/v2/pokemon/', function (error, response, body) {
    //     if (!error && response.statusCode == 200) {
    //         var data = JSON.parse(body)
    //         console.log(data.count) // number poke
    cache.del('teams-list')
    var rnd_poke_id = Math.floor(Math.random() * 807 + 1 )
    pokemon.get_poke(rnd_poke_id).then(poke=>{
      if(poke){
        teams.add_poke(rnd_poke_id,req.params.teamid)
        res.json({success: true, data:poke})
        return
      }
      request('http://pokeapi.co/api/v2/pokemon/'+rnd_poke_id, function (error, response, body) {
          if (!error && response.statusCode == 200) {
            var raw = JSON.parse(body)
            var data = Object.keys(raw)
              .filter(key => request_data.includes(key))
              .reduce((obj, key) => {
                obj[key] = raw[key];
                return obj;
              }, {});
              // saved img into disk
              download(data.sprites.front_default, data.id+'.png', function(){
                delete data.sprites
                data.image = data.id+'.png'
                data.abilities = data.abilities.map(o=>(o.ability.name))
                data.types = data.types.map(o=>(o.type.name))

                pokemon.save_poke(data, req.params.teamid)
                res.json({success: true, data: data})
              });

         }
      })

    })
};


// "name": "bulbasaur",
// sprites: {    "front_default": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png"}
// abilities:[{ability:{name:"..."}],
// base_experience: 0,
// types: [{type{name:""}}]
