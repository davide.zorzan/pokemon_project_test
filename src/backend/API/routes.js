const express = require('express');
const routes = express.Router();

// load controllers
var teams_controller = require('./resources/teamsController')
var poke_controller = require('./resources/pokeController')

//  default api
routes.get('/', (req, res) => {
  res.send('API poke teams, please read README file for more informations');
});

// get all teams or filter by
routes.get('/api/teams', teams_controller.team_list)
// add new team
routes.post('/api/team',  teams_controller.team_new)
// edit team by id
routes.put('/api/team/:id', teams_controller.team_edit)
// get single team
routes.get('/api/team/:id',  teams_controller.team_list)
// delete new team
routes.delete('/api/team/:id', teams_controller.team_delete)

routes.get('/api/random-pokemon/:teamid', poke_controller.random_poke)

routes.get('/assets/images/:id', (req, res) => {


})

module.exports = routes;
