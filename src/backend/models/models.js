require('dotenv').config()
const { Pool } = require('pg')
const pool = new Pool({
  connectionString: process.env.DATABASE_URL,
})


// TODO: add and use ORM engine
// TODO: create object model
exports.teams = {
  get_all: async function(){
      const {rows} = await pool.query(
          'SELECT t.*, jsonb_agg(p.*) pokemon FROM product.teams t INNER JOIN product.team_pokemon tp ON t.id=tp.team_id '+
          'INNER JOIN product.pokemon p ON p.id = tp.poke_id GROUP BY t.id')
      return rows
  },
  insert_team: async function(name){
    var {rows} = await pool.query('INSERT INTO product.teams (name) VALUES ($1) RETURNING id',[name])
    return rows[0].id
  },
  get_team: async function(team_id){
    const {rows} = await pool.query(
      'SELECT t.*, jsonb_agg(p.*) pokemon FROM product.teams t INNER JOIN product.team_pokemon tp ON t.id=tp.team_id '+
      'INNER JOIN product.pokemon p ON p.id = tp.poke_id WHERE t.id = $1 GROUP BY t.id',[team_id])
    return rows[0]
  },
  add_poke: function(pokeid,teamid){
    pool.query('INSERT INTO product.team_pokemon (poke_id,team_id) VALUES ($1,$2)',[pokeid,teamid])
  }
}

exports.pokemon = {
  save_poke: async function(data, teamid){
      pool.query('INSERT INTO product.pokemon (id, name, image, abilities, base_experience, types)'+
                 ' VALUES ($1,$2,$3,$4,$5,$6)', [
                   data.id,
                   data.name,
                   data.image,
                   data.abilities,
                   data.base_experience,
                   data.types
                 ])

      exports.teams.add_poke(data.id, teamid)
  },
  get_poke: async function(id){
    const {rows} = await pool.query('SELECT * FROM product.pokemon WHERE id = $1',[id])
    return rows[0]
  },
  del_poke: function(pokeid, teamid){
    pool.query('DELETE FROM product.team_pokemon WHERE team_id=$1 AND poke_id=$2', [teamid, pokeid])
  }
}
