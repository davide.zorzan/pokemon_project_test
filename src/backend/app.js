'use strict';

const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')

require('dotenv').config()

var routes = require('./API/routes')

// Constants
const PORT = process.env.PORT;
const HOST = process.env.HOST;

// DATABASE

// App
const app = express();
// remov cors block
app.use(cors())
app.use(bodyParser.json())          // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));

app.use('/', routes)



app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
