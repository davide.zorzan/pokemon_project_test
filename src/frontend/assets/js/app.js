var myApp = {
  create_team: function(name){
      return $.ajax({
        url: 'http://localhost/api/team',
        type:'post',
        data:{name: name}
      })
  },

  discover_pokemon: function(id){
    return $.ajax({
      url: 'http://localhost/api/random-pokemon/'+id,
      type:'get',
    })
  },

  get_list: function(){
    return $.ajax({
      url: 'http://localhost/api/teams'
    })
  },

  get_team: function(id){
    return $.ajax({
      url: 'http://localhost/api/team/'+id
    })
  },

  add_poke_list: function(container, res, edit){

    $(container).append('<li class="list-group-item">'+
      (edit?`<button type="button" value="${res.id}" class="btn btn-danger btn-sm float-left">liberates</button>`: '')+
      '<img width="80" src="/images/'+res.image+
      '" class="rounded float-left mr-2" alt="'+res.id+'">'+
      '<span class="text-capitalize align-middle d-inline-block mt-4 ml-2">'+res.name+'</span>'+
      '<span class="badge badge-primary float-right align-middle badge-pill">experience: '+
      res.base_experience+'</span></li>')
  },

  list_row: function(data){
    return data.sort((a,b)=>(a.create_ts > b.create_ts ? -1: 1))
          .map((o)=>('<tr><td><a href="/team/'+o.id+'/edit">'+o.name+'</a></td><td>'+
            o.pokemon.map(p=>('<img width="32" class="rounded float-left" title="'+p.name+'" src="/images/'+p.image+'" />')).join('')+
            '</td><td>'+o.pokemon.reduce((t,p)=>(t+p.base_experience),0)+'</td><td>'+
            o.pokemon.reduce((a,p)=>([...a,...p.types]),[]).filter((v, i, a) => a.indexOf(v) === i)
          .join(', ')+'</td></tr>')).join('')
  },

  liberates_poke: function(teamid,pokeid){
    return $.ajax({
      type: 'put',
      url: 'http://localhost/api/team/'+teamid,
      data: {pokeid:pokeid}
    })
    
  }
}
